using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [Header("Parámetros de la pool")]
    [SerializeField]
    private GameObject objectPrefab;
    [SerializeField]
    private int poolSize = 10;
    [SerializeField]
    private List<GameObject> objectList = new List<GameObject>();

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//

    private void Start()
    {
        AddObjectsToPool(poolSize);
    }

    private void AddObjectsToPool(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject objetoCreado = Instantiate(objectPrefab);
            objetoCreado.SetActive(false);
            objectList.Add(objetoCreado);
        }
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    public GameObject RequestObject()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (!objectList[i].activeSelf)
            {
                objectList[i].SetActive(true);
                return objectList[i];
            }
        }
        AddObjectsToPool(1);
        objectList[objectList.Count - 1].SetActive(true);
        return objectList[objectList.Count - 1];
    }
}
