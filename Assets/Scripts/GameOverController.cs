using System.Collections;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject[] m_textGameOver;
    [SerializeField]
    private GameObject m_youDead;

    [Header("Gesti�n de tiempos")]
    [SerializeField]
    private int m_esperaDesactivacionGameOver;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    private void Awake()
    {
        m_esperaDesactivacionGameOver = 4;
        MostrarTextoGameOver(false);
        MostrarYouDead(false);
    }

    private void Start()
    {
        MostrarTextoGameOver(true);
        StartCoroutine(DesactivacionGameOver());
    }

    //-----------------------------[ CORUTINAS ]-------------------------------//
    private IEnumerator DesactivacionGameOver()
    {
        yield return new WaitForSeconds(m_esperaDesactivacionGameOver);
        MostrarTextoGameOver(false);
        MostrarYouDead(true);
    }

    //-----------------------------[ FUNCIONES ]-------------------------------//
    private void MostrarTextoGameOver(bool activo)
    {
        foreach (GameObject letra in m_textGameOver)
        {
            letra.gameObject.SetActive(activo);
        }
    }

    private void MostrarYouDead(bool activo)
    {
        m_youDead.gameObject.SetActive(activo);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
