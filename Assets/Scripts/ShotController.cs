using System.Collections;
using UnityEngine;

public class ShotController : MonoBehaviour
{
    [Header("Componentes")]
    [SerializeField]
    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;
    private HitboxController m_HitboxController;

    [Header("Parámetros del disparo")]
    [SerializeField]
    private float m_ShotSpeed = 5f;
    private int m_DestroyTime = 2;
    private float m_DestroyTimeWhenCollides = 0.3f;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    private void OnEnable()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_Animator.enabled = true;
        m_HitboxController = GetComponent<HitboxController>();
        StartCoroutine(DestruirShot());
    }

    public void Init(int shotDamage, Vector3 playerTransform)
    {
        if (m_Rigidbody == null) m_Rigidbody = GetComponent<Rigidbody2D>();

        if (m_HitboxController == null) m_HitboxController = GetComponent<HitboxController>();

        Vector3 direction = (playerTransform - transform.position).normalized;
        transform.right = direction;
        m_Rigidbody.velocity = direction * m_ShotSpeed;
        m_HitboxController.Damage = shotDamage;

    }

    //-------------------------------[ CORUTINAS ]-------------------------------//
    private IEnumerator DestruirShot()
    {
        yield return new WaitForSeconds(m_DestroyTime);
        m_Animator.enabled = false;
        gameObject.SetActive(false);
    }

    private IEnumerator ImpactoShot()
    {
        m_Animator.Play("ShootEnd");
        yield return new WaitForSeconds(m_DestroyTimeWhenCollides);
        m_Animator.enabled = false;
        gameObject.SetActive(false);
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            m_Rigidbody.velocity = Vector2.zero;
            StartCoroutine(ImpactoShot());
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
