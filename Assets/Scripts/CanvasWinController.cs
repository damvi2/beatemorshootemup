using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasWinController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private TextMeshProUGUI m_enemigosDerrotados;
    [SerializeField]
    private TextMeshProUGUI m_volverAJugar;
    [SerializeField]
    private Button m_btnStart;

    [Header("Scriptable Objects")]
    [SerializeField]
    private OleadaFinalSO m_Oleadafinal;

    [Header("Gesti�n de tiempos")]
    [SerializeField]
    private int m_esperaOleadaFinal;

    private void Awake()
    {
        m_esperaOleadaFinal = 4;
        m_volverAJugar.text = "Quieres volver a jugar?";
        m_volverAJugar.gameObject.SetActive(false);
        m_enemigosDerrotados.text = "Has derrotado a " + m_Oleadafinal.EnemigosDerrotados + " enemigos.";
        m_enemigosDerrotados.gameObject.SetActive(false);
        m_btnStart.gameObject.SetActive(false);
    }

    private void Start()
    {
        StartCoroutine(MostrarOleadaFinal());
    }

    private IEnumerator MostrarOleadaFinal()
    {
        yield return new WaitForSeconds(m_esperaOleadaFinal);
        m_volverAJugar.gameObject.SetActive(true);
        m_enemigosDerrotados.gameObject.SetActive(true);
        m_btnStart.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
