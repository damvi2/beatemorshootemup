using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene("01_PlayGame");
    }
}
