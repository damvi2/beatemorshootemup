using System.Collections;
using Unity.Mathematics;
using UnityEngine;

public class EnemicRangedController : MonoBehaviour
{
    [Header("Componentes")]
    private Animator m_Animator;
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody;

    [Header("Par�metros del Ranged Enemy")]
    [SerializeField]
    private EnemiesSO rangedEnemySO;
    [SerializeField]
    private EnemiesSO[] m_configRangedEnemy;
    [SerializeField]
    private int m_CurrentHP;
    private float m_Speed;
    private enum m_Estados { NONE, IDLE, PATROL, CHASE, SHOOT, HURT, DEAD };
    [SerializeField]
    private m_Estados m_EstadoActual;

    [Header("Scriptable Objects")]
    [SerializeField]
    private OleadaFinalSO m_oleadaFinal;

    [Header("Referencias a objetos")]
    private Transform m_PlayerTransform;
    [SerializeField]
    private Transform[] m_Waypoints;

    [Header("Rutas")]
    [SerializeField]
    private Transform[] m_waypointsRuta1;
    [SerializeField]
    private Transform[] m_waypointsRuta2;
    [SerializeField]
    private Transform[] m_waypointsRuta3;
    private int m_CurrentWaipoint;

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoEsperaEnPatrol;
    [SerializeField]
    private float m_TiempoEsperaDisparo;
    [SerializeField]
    private float m_TiempoEsperaDisparoArea;

    [Header("Disparo")]
    [SerializeField]
    private GameObject m_ShotPool;
    [SerializeField]
    private Transform m_ShotPosition;
    [SerializeField]
    private int m_ShotDamage;

    [Header("�reas de detecci�n")]
    [SerializeField]
    private DetectionArea m_AreaPerseguir; //Evento del OnTriggerEnter de las areas de deteccion
    [SerializeField]
    private DetectionArea m_SaleDelAreaPerseguir;
    [SerializeField]
    private DetectionArea m_AreaDisparar;  //Evento del OnTriggerEnter de las areas de deteccion
    [SerializeField]
    private DetectionArea m_SaleDelAreaDisparo;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoInt HeMuerto;

    [Header("Variables de control")]
    private bool heEntradoArea;

    [Header("Corutinas")]
    private Coroutine m_esperandoEnPatrol;
    private Coroutine m_disparando;
    private Coroutine m_esperaDisparo;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        if (m_oleadaFinal.OleadaFinal < 6)
        {
            rangedEnemySO = m_configRangedEnemy[0];
        }
        else
        {
            rangedEnemySO = m_configRangedEnemy[1];
        }

        m_CurrentHP = rangedEnemySO.HP;
        m_Speed = rangedEnemySO.Speed;
        m_ShotDamage = rangedEnemySO.Damage;
        heEntradoArea = false;

        //Suscripcion eventos
        m_AreaPerseguir.OnDetected += EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit += DejaDePerseguir;
        m_AreaDisparar.OnDetected += Disparo;
        m_SaleDelAreaDisparo.OnExit += DejaDeDisparar;

        DecidirRuta();
        IniciarEstado(m_Estados.IDLE);
    }

    private void OnDisable()
    {
        //Nos desuscribimos porque sino al cambiar de escena se mantienen las suscripciones
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaDisparar.OnDetected -= Disparo;
        m_SaleDelAreaDisparo.OnExit -= DejaDeDisparar;
        StopAllCoroutines();
    }

    private void Update()
    {
        ActualizarEstado();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//

    private void CambiarEstado(m_Estados nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == m_Estados.DEAD) return;

        FinalizarEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(m_Estados nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_esperandoEnPatrol = StartCoroutine(EsperandoEnPatrol()); //Nos guardamos la corutina para poder hacer el StopCoroutine, sino no funciona
                m_Animator.Play("RangedEnemyIdle");
                break;

            case m_Estados.PATROL:
                m_Animator.Play("RangedEnemyRun");
                break;

            case m_Estados.CHASE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("RangedEnemyRun");
                break;

            case m_Estados.SHOOT:
                m_Rigidbody.velocity = Vector2.zero;
                m_esperaDisparo = StartCoroutine(EsperaDisparoArea());
                m_Animator.Play("RangedEnemyShoot");
                break;

            case m_Estados.HURT:
                m_SpriteRenderer.color = Color.red;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("RangedEnemyHurt");
                break;

            case m_Estados.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("RangedEnemyDead");
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual != m_Estados.DEAD)
        {
            switch (m_EstadoActual)
            {
                case m_Estados.PATROL:
                    if (math.distancesq(transform.position, m_Waypoints[m_CurrentWaipoint].position) >= 0.01f) //Es como el Vector2.Distance pero sqr es mas optimo
                    {
                        m_Rigidbody.velocity = (m_Waypoints[m_CurrentWaipoint].position - transform.position).normalized * m_Speed;

                        Encarar(m_Rigidbody.velocity);
                    }
                    else
                    {
                        CambiarEstado(m_Estados.IDLE); //Si he llegado al waypoint, cambiamos a IDLE
                    }

                    break;

                case m_Estados.CHASE:
                    m_Rigidbody.velocity = (m_PlayerTransform.position - transform.position).normalized * m_Speed;

                    Encarar(m_Rigidbody.velocity);

                    break;
            }
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.SHOOT:
                if (m_esperaDisparo != null)
                {
                    StopCoroutine(m_esperaDisparo);
                }
                if (m_disparando != null)
                {
                    StopCoroutine(m_disparando);
                }
                break;
            case m_Estados.IDLE:
                if (m_esperandoEnPatrol != null)
                {
                    StopCoroutine(m_esperandoEnPatrol);
                }
                break;
            case m_Estados.HURT:
                m_SpriteRenderer.color = Color.white;
                break;
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator EsperandoEnPatrol()
    {
        yield return new WaitForSeconds(m_TiempoEsperaEnPatrol);
        m_CurrentWaipoint++;

        if (m_CurrentWaipoint == m_Waypoints.Length)
        {
            m_CurrentWaipoint = 0;
        }
        CambiarEstado(m_Estados.PATROL);
    }

    private IEnumerator Disparando(Transform PlayerTransform)
    {
        while (true)
        {
            Encarar(PlayerTransform.position - transform.position);
            GameObject disparo = m_ShotPool.gameObject.GetComponent<ObjectPool>().RequestObject();

            disparo.transform.position = m_ShotPosition.position;

            disparo.SetActive(true);
            disparo.GetComponent<ShotController>().Init(m_ShotDamage, PlayerTransform.position);
            yield return new WaitForSeconds(m_TiempoEsperaDisparo);
        }
    }

    private IEnumerator EsperaDisparoArea()
    {
        yield return new WaitForSeconds(m_TiempoEsperaDisparoArea);
        m_disparando = StartCoroutine(Disparando(m_PlayerTransform));
    }

    //-------------------------------[ DELEGADOS AREAS DETECCION ]-------------------------------//

    public void EstadoPerseguir(Transform PlayerTransform)
    {
        m_PlayerTransform = PlayerTransform;
        heEntradoArea = false;
        CambiarEstado(m_Estados.CHASE);
    }

    public void DejaDePerseguir()
    {
        CambiarEstado(m_Estados.IDLE);
    }

    public void Disparo(Transform PlayerTransform)
    {
        heEntradoArea = true;
        CambiarEstado(m_Estados.SHOOT);
    }

    public void DejaDeDisparar()
    {
        CambiarEstado(m_Estados.CHASE);
    }

    //-----------------------------[ EVENTOS ANIMACIONES ]-------------------------------//
    public void FinalizaHurt()
    {
        if (heEntradoArea)
        {
            CambiarEstado(m_Estados.SHOOT);
        }
        else
        {
            CambiarEstado(m_Estados.IDLE);
        }
    }

    private void Die()
    {
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaDisparar.OnDetected -= Disparo;
        m_SaleDelAreaDisparo.OnExit -= DejaDeDisparar;
        StopAllCoroutines();
        HeMuerto.Raise(1);
        gameObject.SetActive(false);
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//

    private void DecidirRuta()
    {
        int rutaRandom = UnityEngine.Random.Range(1, 4);

        switch (rutaRandom)
        {
            case 1:
                m_Waypoints = m_waypointsRuta1;
                break;
            case 2:
                m_Waypoints = m_waypointsRuta2;
                break;
            case 3:
                m_Waypoints = m_waypointsRuta3;
                break;
        }

        int posicionRandom = UnityEngine.Random.Range(0, m_Waypoints.Length);
        m_CurrentWaipoint = posicionRandom;
        transform.position = m_Waypoints[m_CurrentWaipoint].position;
    }

    private void Encarar(Vector3 direccion) //No sirve girar el SpriteRenderer porque entonces los colliders no se giran con el enemigo
    {
        if (direccion.x < 0) //Si la x es menor a 0, rotamos
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            transform.eulerAngles = Vector3.zero;
        }
    }

    private void DisminuirVida(int Damage)
    {
        if (m_CurrentHP > 0)
        {
            m_CurrentHP -= Damage;
            CambiarEstado(m_Estados.HURT);
        }

        if (m_CurrentHP <= 0)
        {
            m_CurrentHP = 0;
            CambiarEstado(m_Estados.DEAD);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Hitboxs"))
        {
            int CollisionDamage = collision.gameObject.GetComponent<HitboxController>().Damage;
            DisminuirVida(CollisionDamage);
        }
    }

    private void OnDestroy()
    {
        //Nos desuscribimos porque sino al cambiar de escena se mantienen las suscripciones
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaDisparar.OnDetected -= Disparo;
        m_SaleDelAreaDisparo.OnExit -= DejaDeDisparar;
    }
}
