using System.Collections;
using UnityEngine;

public class EnemicMeleeController : MonoBehaviour
{
    [Header("Componentes")]
    private Animator m_Animator;
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody;

    [Header("Par�metros del Melee Enemy")]
    [SerializeField]
    private EnemiesSO meleeEnemySO;
    [SerializeField]
    private EnemiesSO[] m_configMeleeEnemy;
    [SerializeField]
    private int m_CurrentHP;
    private float m_Speed;

    private enum m_Estados { NONE, IDLE, CHASE, ATTACK, HURT, DEAD };
    [SerializeField]
    private m_Estados m_EstadoActual;

    [Header("Scriptable Objects")]
    [SerializeField]
    private OleadaFinalSO m_oleadaFinal;

    [Header("Referencias a objetos")]
    private Transform m_PlayerTransform;
    [SerializeField]
    private GameObject m_meleeHitbox;
    private HitboxController m_HitboxController;

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoEsperaAtaque;

    [Header("�reas de detecci�n")]
    [SerializeField]
    private DetectionArea m_AreaPerseguir;
    [SerializeField]
    private DetectionArea m_SaleDelAreaPerseguir;
    [SerializeField]
    private DetectionArea m_AreaAtacar;
    [SerializeField]
    private DetectionArea m_SaleDelAreaAtacar;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoInt HeMuerto;

    [Header("Variables de control")]
    private bool heEntradoAreaAtaque;
    private bool heEntradoAreaPerseguir;

    [Header("Corutinas")]
    private Coroutine m_esperaAtaque;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_HitboxController = m_meleeHitbox.gameObject.GetComponent<HitboxController>();
    }

    private void OnEnable()
    {
        if (m_oleadaFinal.OleadaFinal < 6)
        {
            meleeEnemySO = m_configMeleeEnemy[0];
        }
        else
        {
            meleeEnemySO = m_configMeleeEnemy[1];
        }

        m_CurrentHP = meleeEnemySO.HP;
        m_Speed = meleeEnemySO.Speed;
        m_HitboxController.Damage = meleeEnemySO.Damage;
        heEntradoAreaAtaque = false;
        heEntradoAreaPerseguir = false;

        //Suscripcion eventos
        m_AreaPerseguir.OnDetected += EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit += DejaDePerseguir;
        m_AreaAtacar.OnDetected += Atacar;
        m_SaleDelAreaAtacar.OnExit += DejaDeAtacar;

        gameObject.transform.position = new Vector3(Random.Range(-8f, 8f), Random.Range(-3.3f, 2.6f));

        IniciarEstado(m_Estados.IDLE);
    }

    private void OnDisable()
    {
        //Nos desuscribimos porque sino al cambiar de escena se mantienen las suscripciones
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaAtacar.OnDetected -= Atacar;
        m_SaleDelAreaAtacar.OnExit -= DejaDeAtacar;
        StopAllCoroutines();
    }

    private void Update()
    {
        ActualizarEstado();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//

    private void CambiarEstado(m_Estados nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == m_Estados.DEAD) return;

        FinalizarEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(m_Estados nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemyIdle");
                break;

            case m_Estados.CHASE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemyRun");
                break;

            case m_Estados.ATTACK:
                m_Rigidbody.velocity = Vector2.zero;
                m_esperaAtaque = StartCoroutine(WaitAtaque());
                break;

            case m_Estados.HURT:
                m_Rigidbody.velocity = Vector2.zero;
                m_SpriteRenderer.color = Color.red;
                m_Animator.Play("MeleeEnemyHurt");
                break;

            case m_Estados.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemyDead");
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.CHASE:
                m_Rigidbody.velocity = (m_PlayerTransform.position - transform.position).normalized * m_Speed;

                Encarar(m_Rigidbody.velocity);

                break;
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.ATTACK:
                StopCoroutine(m_esperaAtaque);
                break;
            case m_Estados.HURT:
                m_SpriteRenderer.color = Color.white;
                break;
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator WaitAtaque()
    {
        while (true)
        {
            /*El "0" es la layer de la animacion; el "0f" es el tiempo (en segundos) en el que
            deseamos iniciar la reproduccion de la animacion (en el segundo 0), es decir, desde el principio*/
            m_Animator.Play("MeleeEnemyAttack", 0, 0f);
            yield return new WaitForSeconds(m_TiempoEsperaAtaque);
        }
    }

    //-------------------------------[ DELEGADOS AREAS DETECCION ]-------------------------------//

    public void EstadoPerseguir(Transform PlayerTransform)
    {
        heEntradoAreaPerseguir = true;
        m_PlayerTransform = PlayerTransform;
        CambiarEstado(m_Estados.CHASE);
    }

    public void DejaDePerseguir()
    {
        heEntradoAreaPerseguir = false;
        CambiarEstado(m_Estados.IDLE);
    }

    public void Atacar(Transform PlayerTransform)
    {
        heEntradoAreaAtaque = true;
        CambiarEstado(m_Estados.ATTACK);
    }

    public void DejaDeAtacar()
    {
        heEntradoAreaAtaque = false;
        CambiarEstado(m_Estados.CHASE);
    }

    //-----------------------------[ EVENTOS ANIMACIONES ]-------------------------------//

    public void FinalizaHurt()
    {
        ComprobarEstadoAlQueCambiar();
    }

    private void Die()
    {
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaAtacar.OnDetected -= Atacar;
        m_SaleDelAreaAtacar.OnExit -= DejaDeAtacar;
        StopAllCoroutines();
        HeMuerto.Raise(1);
        gameObject.SetActive(false);
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//

    private void Encarar(Vector3 direccion) //No sirve girar el SpriteRenderer porque entonces los colliders no se giran con el enemigo
    {
        if (direccion.x < 0) //Si la x es menor a 0, rotamos
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            transform.eulerAngles = Vector3.zero;
        }
    }

    private void DisminuirVida(int Damage)
    {
        if (m_CurrentHP > 0)
        {
            m_CurrentHP -= Damage;
            CambiarEstado(m_Estados.HURT);
        }

        if (m_CurrentHP <= 0)
        {
            m_CurrentHP = 0;
            CambiarEstado(m_Estados.DEAD);
        }
    }

    private void ComprobarEstadoAlQueCambiar()
    {
        if (heEntradoAreaAtaque)
        {
            CambiarEstado(m_Estados.ATTACK);
        }
        else if (!heEntradoAreaAtaque)
        {
            CambiarEstado(m_Estados.IDLE);

            if (heEntradoAreaPerseguir)
            {
                CambiarEstado(m_Estados.CHASE);
            }
            else if (!heEntradoAreaPerseguir)
            {
                CambiarEstado(m_Estados.IDLE);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Hitboxs"))
        {
            int CollisionDamage = collision.gameObject.GetComponent<HitboxController>().Damage;
            DisminuirVida(CollisionDamage);
        }
    }

    private void OnDestroy()
    {
        //Nos desuscribimos porque sino al cambiar de escena se mantienen las suscripciones
        m_AreaPerseguir.OnDetected -= EstadoPerseguir;
        m_SaleDelAreaPerseguir.OnExit -= DejaDePerseguir;
        m_AreaAtacar.OnDetected -= Atacar;
        m_SaleDelAreaAtacar.OnExit -= DejaDeAtacar;
    }
}
