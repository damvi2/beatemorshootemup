using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private Image m_ImagenBarra;
    [SerializeField]
    private TextMeshProUGUI m_TextoOleada;
    [SerializeField]
    private TextMeshProUGUI m_TituloOleada;
    [SerializeField]
    private Image m_BackgroundOleada;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    private void Awake()
    {
        m_ImagenBarra.fillAmount = 1;
    }

    //-------------------------------[ EVENTOS ]-------------------------------//
    public void ActualizarVida(float vidaJugador, float vidaMaxJugador)
    {
        m_ImagenBarra.fillAmount = vidaJugador / vidaMaxJugador;
    }

    public void ActualizarOleada(int numOleada)
    {
        m_TituloOleada.text = "Oleada " + numOleada;
        m_TextoOleada.text = m_TituloOleada.text;
    }

    public void MostrarTitulo(int mostrar)
    {
        bool mostramosTitulo = (mostrar != 0); //mostrar da 1 para que se muestre titulo y 0 para que no se muestre
        m_TituloOleada.enabled = mostramosTitulo;
        m_BackgroundOleada.enabled = mostramosTitulo;
    }
}
