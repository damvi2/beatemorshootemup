using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input; //Input
    private InputAction m_PlayerMovement; //Input de movimiento
    private InputAction m_PlayerAttack1; //Input de ataque1
    private InputAction m_PlayerAttack2; //Input de ataque2

    [Header("Componentes")]
    private Animator m_Animator; 
    private SpriteRenderer m_SpriteRenderer; 
    private Rigidbody2D m_Rigidbody;
    private HitboxController m_HitboxController;

    [Header("Eventos")]
    [SerializeField]
    public GEGenericoDoble<float, float> UpdateLife;
    [SerializeField]
    public GEGenericoInt HeMuerto;

    [Header("Ataques")]
    [SerializeField]
    private int m_DamageAttack1 = 7;
    [SerializeField]
    private int m_DamageAttack2 = 2;
    [SerializeField]
    private int m_DamageComboWeak = 10;
    [SerializeField]
    private int m_DamageComboStrong = 8;
    private bool m_ComboDisponible = false;

    [Header("Parámetros jugador")]
    [SerializeField]
    private float m_MaxHP = 20;
    [SerializeField]
    private float m_CurrentHP;
    [SerializeField]
    private float m_Speed = 5;
    private bool m_MiraAIzquierda = false; //El jugador comienza mirando a la derecha

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    void Awake()
    {
        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        m_PlayerMovement = m_Input.FindActionMap("PlayerMovement").FindAction("Movement");
        m_PlayerAttack1 = m_Input.FindActionMap("PlayerMovement").FindAction("Attack1");
        m_PlayerAttack1.performed += Atacar;
        m_PlayerAttack2 = m_Input.FindActionMap("PlayerMovement").FindAction("Attack2");
        m_PlayerAttack2.performed += Atacar2;

        m_Input.FindActionMap("PlayerMovement").Enable();

        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_HitboxController = gameObject.transform.GetChild(0).gameObject.GetComponent<HitboxController>();

        m_CurrentHP = m_MaxHP;

        gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    void Start()
    {
        IniciarEstado(m_Estados.IDLE);
    }

    void Update()
    {
        ActualizarEstado();
    }


    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//

    /*Esta maquina funciona con un enum de estados y un switch para la gestion de la logica de cada uno.
    Imprescindible utilizar sus funciones de cambio, actualizacion e inicio y salida de cada estado.
    El estado 'None' simplemente existe porque tenemos que hacer el 'init' del primer estado
    y hace falta que la variable no tenga el valor igual al primero de ellos (idle).*/
    private enum m_Estados { NONE, IDLE, RUN, ATTACK1, ATTACK2, COMBOWEAK, COMBOSTRONG, COMBO, HURT, DEAD };
    [SerializeField]
    private m_Estados m_EstadoActual;

    private void CambiarEstado(m_Estados nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == m_Estados.DEAD) return;

        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(m_Estados nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerIdle");
                break;

            case m_Estados.RUN:
                m_Animator.Play("PlayerRun");
                break;

            case m_Estados.ATTACK1:
                m_HitboxController.Damage = m_DamageAttack1;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerAttack1");
                break;

            case m_Estados.ATTACK2:
                m_HitboxController.Damage = m_DamageAttack2;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerAttack2");
                break;

            case m_Estados.COMBOWEAK:
                m_HitboxController.Damage = m_DamageComboWeak;
                m_ComboDisponible = false;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerAttackWeakStrong");
                break;

            case m_Estados.COMBOSTRONG:
                m_HitboxController.Damage = m_DamageComboStrong;
                m_ComboDisponible = false;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerAttackStrongWeak");
                break;

            case m_Estados.HURT:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerHurt");
                break;

            case m_Estados.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("PlayerDead");
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                if (m_PlayerMovement.ReadValue<Vector2>().x != 0 || m_PlayerMovement.ReadValue<Vector2>().y != 0) //Si el jugador se mueve en x o en y, cambiamos al estado RUN
                    CambiarEstado(m_Estados.RUN);
                break;

            case m_Estados.RUN:
                Vector2 movimiento = m_PlayerMovement.ReadValue<Vector2>();

                m_Rigidbody.velocity = new Vector2(movimiento.x, movimiento.y) * m_Speed;

                if (movimiento.x < 0) //Gestionamos el flipeo del personaje en x
                {
                    if (!m_MiraAIzquierda) flipX(true);

                }
                else if (movimiento.x > 0)
                {
                    if (m_MiraAIzquierda) flipX(false);
                }

                if (m_Rigidbody.velocity == Vector2.zero)
                    CambiarEstado(m_Estados.IDLE);
                break;

            case m_Estados.ATTACK1:
                break;

            case m_Estados.ATTACK2:
                break;

            case m_Estados.COMBOWEAK:
                break;

            case m_Estados.COMBOSTRONG:
                break;
            case m_Estados.HURT:
                m_SpriteRenderer.color = Color.red;
                break;

            default:
                break;
        }
    }

    //-----------------------------[ EVENTOS ANIMACIONES ]-------------------------------//

    public void InicializarCombo() //Metodos que se llaman desde los eventos de las animaciones
    {
        m_ComboDisponible = true;
    }

    public void FinalizarCombo() //Metodos que se llaman desde los eventos de las animaciones
    {
        m_ComboDisponible = false;
    }

    public void FinalizarHit() //Metodos que se llaman desde los eventos de las animaciones
    {
        CambiarEstado(m_Estados.IDLE);
    }

    public void FinalizaHurt()
    {
        m_SpriteRenderer.color = Color.white;
        CambiarEstado(m_Estados.IDLE);
    }

    public void Die()
    {
        HeMuerto.Raise(1);
    }

    //-----------------------------[ ATAQUES ]-------------------------------//
    private void Atacar(InputAction.CallbackContext actionContext)
    {
        //Debug.Log(actionContext.control.displayName); //Esto sirve para ver el nombre del input que estamos presionando
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                CambiarEstado(m_Estados.ATTACK1);
                break;

            case m_Estados.RUN:
                CambiarEstado(m_Estados.ATTACK1);
                break;

            case m_Estados.ATTACK1:
                break;

            case m_Estados.ATTACK2:
                if (m_ComboDisponible) CambiarEstado(m_Estados.COMBOSTRONG);
                break;

            default:
                break;
        }
    }

    private void Atacar2(InputAction.CallbackContext actionContext)
    {
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                CambiarEstado(m_Estados.ATTACK2);
                break;

            case m_Estados.RUN:
                CambiarEstado(m_Estados.ATTACK2);
                break;

            case m_Estados.ATTACK1:
                if (m_ComboDisponible) CambiarEstado(m_Estados.COMBOWEAK);
                break;

            case m_Estados.ATTACK2:
                break;

            default:
                break;
        }
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    private void flipX(bool valor) //No sirve girar el SpriteRenderer porque entonces los colliders no se giran con el jugador
    {
        m_MiraAIzquierda = valor;
        Vector2 NewLocalScale = transform.localScale;
        NewLocalScale = new Vector2(-NewLocalScale.x, NewLocalScale.y);
        transform.localScale = NewLocalScale;
    }

    private void DisminuirVida(float Damage)
    {
        if (m_CurrentHP > 0)
        {
            m_CurrentHP -= Damage;
            UpdateLife.Raise(m_CurrentHP, m_MaxHP);
            CambiarEstado(m_Estados.HURT);
        }

        if (m_CurrentHP <= 0)
        {
            UpdateLife.Raise(0, m_MaxHP);
            m_CurrentHP = 0;
            CambiarEstado(m_Estados.DEAD);
        }
    }

    public void AumentarVida(int porcentageVida)
    {
        m_CurrentHP += (m_MaxHP - m_CurrentHP) * ((float)porcentageVida / 100);

        if (m_CurrentHP > m_MaxHP)
        {
            m_CurrentHP = m_MaxHP;
        }

        UpdateLife.Raise(m_CurrentHP, m_MaxHP);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Shots"))
        {
            float CollisionDamage = collision.gameObject.GetComponent<HitboxController>().Damage;
            DisminuirVida(CollisionDamage);
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("HitboxsEnemies"))
        {
            float CollisionDamage = collision.gameObject.GetComponent<HitboxController>().Damage;
            DisminuirVida(CollisionDamage);
        }
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("PlayerMovement").FindAction("Attack1").performed -= Atacar;
        m_Input.FindActionMap("PlayerMovement").FindAction("Attack2").performed -= Atacar2;
        m_Input.FindActionMap("PlayerMovement").Disable();
        StopAllCoroutines();
    }
}
