using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveSpawnerController : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField]
    private GameObject poolMeleeEnemies;
    [SerializeField]
    private GameObject poolRangedEnemies;

    [Header("Scriptable Objects")]
    [SerializeField]
    private OleadaFinalSO m_oleadaFinal;
    [SerializeField]
    private ConfigWaveSO[] m_configWaves;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoInt CambiarOleada;
    [SerializeField]
    private GEGenericoInt MostrarTitulo;
    [SerializeField]
    private GEGenericoInt AumentarVidaPlayer;

    [Header("Variables de gesti�n")]
    private int enemigosTodaviaVivos;
    private int actualConfig;
    private bool canSpawn;

    [Header("Corutinas")]
    Coroutine m_crearEnemigos;
    Coroutine m_activarTitulo;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    private void Awake()
    {
        InitValues();
    }

    private void Start()
    {
        ComienzaPartida();
    }

    private void InitValues()
    {
        actualConfig = 0;
        canSpawn = true;
    }

    //-----------------------------[ FUNCIONES SPAWNER ]-------------------------------//

    private void ComienzaPartida()
    {
        CambiarOleada.Raise(m_configWaves[actualConfig].NumOleada);
        m_oleadaFinal.OleadaFinal = m_configWaves[actualConfig].NumOleada;
        //print("START OLEADA: " + m_configWaves[actualConfig].NumOleada);
        m_oleadaFinal.EnemigosDerrotados = 0;

        m_crearEnemigos = StartCoroutine(CrearEnemigo());
    }

    private void CalcularProbabilidad()
    {
        float probabilidadRanged = m_configWaves[actualConfig].ProbabilityRanged;

        float probabilidadRandom = UnityEngine.Random.value; //Da un valor entre 0 y 1

        if (probabilidadRandom <= probabilidadRanged)
        {
            InstanciamosRanged();
        }
        else
        {
            InstanciamosMelee();
        }
    }

    private void InstanciamosRanged()
    {
        poolRangedEnemies.gameObject.GetComponent<ObjectPool>().RequestObject();
    }

    private void InstanciamosMelee()
    {
        poolMeleeEnemies.gameObject.GetComponent<ObjectPool>().RequestObject();
    }

    public void DisminuimosEnemigosVivos(int cantidad)
    {
        enemigosTodaviaVivos -= cantidad;
        m_oleadaFinal.EnemigosDerrotados += cantidad;
        //print("QUEDAN VIVOS: " + enemigosTodaviaVivos);

        ComprobarCambioOleada();
        ComprobarWin();
    }

    private void ComprobarCambioOleada()
    {
        if (enemigosTodaviaVivos == 0 && m_oleadaFinal.OleadaFinal < 10 && canSpawn)
        {
            canSpawn = false;
            StopCoroutine(m_crearEnemigos);
            actualConfig++;
            CambiarOleada.Raise(m_configWaves[actualConfig].NumOleada); //Avisamos al canvas para que se actualice
            //print("CAMBIAMOS A OLEADA: " + m_configWaves[actualConfig].NumOleada);
            m_oleadaFinal.OleadaFinal = m_configWaves[actualConfig].NumOleada;
            AumentarVidaPlayer.Raise(20);
            m_crearEnemigos = StartCoroutine(CrearEnemigo());
        }
    }

    private void ComprobarWin()
    {
        if (enemigosTodaviaVivos == 0 && m_oleadaFinal.OleadaFinal == 10)
        {
            SceneManager.LoadScene("03_Win");
        }
    }

    //-----------------------------[ CORUTINAS ]-------------------------------//
    private IEnumerator CrearEnemigo()
    {
        enemigosTodaviaVivos = m_configWaves[actualConfig].CantidadEnemigos;
        //print("EMPEZAMOS CON " + enemigosTodaviaVivos + " ENEMIGOS VIVOS");
        canSpawn = true;
        m_activarTitulo = StartCoroutine(ActivarTitulo());
        yield return m_activarTitulo; //Esto espera a que termine la corutina para hacer el for

        for (int i = 0; i < m_configWaves[actualConfig].CantidadEnemigos; i++)
        {
            CalcularProbabilidad();
            yield return new WaitForSeconds(m_configWaves[actualConfig].TiempoEsperaCreacion);
        }
    }

    public IEnumerator ActivarTitulo()
    {
        yield return new WaitForSeconds(0.5f);
        MostrarTitulo.Raise(1);
        yield return new WaitForSeconds(2);
        MostrarTitulo.Raise(0);
        yield return new WaitForSeconds(1);
        canSpawn = true;
    }

    //-----------------------------[ EVENTOS ]-------------------------------//
    public void PlayerHaMuerto(int vivo)
    {
        SceneManager.LoadScene("02_GameOver");
    }

    //-----------------------------[ FUNCIONES ]-------------------------------//
    private void OnDestroy()
    {
        if (m_crearEnemigos != null) StopCoroutine(m_crearEnemigos);
        if (m_activarTitulo != null) StopCoroutine(m_activarTitulo);
    }
}
