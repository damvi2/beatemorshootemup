using System;
using UnityEngine;

public class DetectionArea : MonoBehaviour
{
    public Action<Transform> OnDetected; //Delegado OnDetected que le pasamos el Transform de con lo que colisionamos
    public Action OnExit; //Cuando lo que colisionamos sale del area


    //-----------------------------[ FUNCIONES ]-------------------------------//
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player")) OnDetected?.Invoke(collision.gameObject.transform);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player")) OnExit?.Invoke();
    }
}
