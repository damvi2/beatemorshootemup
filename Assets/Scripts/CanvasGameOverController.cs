using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasGameOverController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private TextMeshProUGUI m_oleadaFinal;
    [SerializeField]
    private TextMeshProUGUI m_enemigosDerrotados;
    [SerializeField]
    private Button m_btnStart;

    [Header("Scriptable Objects")]
    [SerializeField]
    private OleadaFinalSO m_Oleadafinal;

    [Header("Gesti�n de tiempos")]
    [SerializeField]
    private int m_esperaOleadaFinal;

    //-------------------------------[ INICIALIZAMOS VARIABLES Y COMPONENTES ]-------------------------------//
    private void Awake()
    {
        m_esperaOleadaFinal = 4;
        m_oleadaFinal.text = "Te has quedado en la oleada " + m_Oleadafinal.OleadaFinal + ".";
        m_oleadaFinal.gameObject.SetActive(false);
        m_enemigosDerrotados.text = "Has derrotado a " + m_Oleadafinal.EnemigosDerrotados + " enemigos.";
        m_enemigosDerrotados.gameObject.SetActive(false);
        m_btnStart.gameObject.SetActive(false);
    }

    private void Start()
    {
        StartCoroutine(MostrarOleadaFinal());
    }

    //-----------------------------[ CORUTINAS ]-------------------------------//
    private IEnumerator MostrarOleadaFinal()
    {
        yield return new WaitForSeconds(m_esperaOleadaFinal);
        m_oleadaFinal.gameObject.SetActive(true);
        m_enemigosDerrotados.gameObject.SetActive(true);
        m_btnStart.gameObject.SetActive(true);
    }

    //-----------------------------[ FUNCIONES ]-------------------------------//
    private void OnDestroy()
    {
        StopAllCoroutines();
    }

}
