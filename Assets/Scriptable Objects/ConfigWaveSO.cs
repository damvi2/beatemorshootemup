using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/Wave Configuration")]
public class ConfigWaveSO : ScriptableObject
{
    [SerializeField]
    private int numOleada;
    [SerializeField]
    private int cantidadEnemigos;
    [SerializeField]
    private float tiempoEsperaCreacion;
    [SerializeField]
    private float probabilityMeleeEnemy;
    [SerializeField]
    private float probabilityRangedEnemy;

    public int NumOleada { get { return numOleada; } }
    public int CantidadEnemigos {  get {  return cantidadEnemigos; } }
    public float TiempoEsperaCreacion { get { return tiempoEsperaCreacion;} } 
    public float ProbabilityMelee { get { return probabilityMeleeEnemy; } }
    public float ProbabilityRanged { get {  return probabilityRangedEnemy; } }
}
