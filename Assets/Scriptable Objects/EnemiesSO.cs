using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/Enemies")]
public class EnemiesSO : ScriptableObject
{
    [SerializeField]
    private int m_hp;
    [SerializeField]
    private float m_speed;
    [SerializeField]
    private int m_damage;

    public int HP { get { return m_hp; } }
    public float Speed { get { return m_speed; } }
    public int Damage { get { return m_damage; } }
}