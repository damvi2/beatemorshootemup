using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/Oleada final")]
public class OleadaFinalSO : ScriptableObject
{
    [SerializeField]
    private int oleadaFinal;
    [SerializeField]
    private int enemigosDerrotados;

    public int OleadaFinal { get { return oleadaFinal; } set { oleadaFinal = value; } }
    public int EnemigosDerrotados { get { return enemigosDerrotados; } set { enemigosDerrotados = value; } }
}
