using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Float, float")]
public class GEDobleFloat : GEGenericoDoble<float, float> { }
