using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GEListenerGenericoDoble<T, P> : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GEGenericoDoble<T, P> Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<T, P> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T parameter, P parameter2)
    {
        Response.Invoke(parameter, parameter2);
    }
}
