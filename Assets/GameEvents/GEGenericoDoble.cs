using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GEGenericoDoble<T, P> : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GEListenerGenericoDoble<T, P>> eventListeners =
        new List<GEListenerGenericoDoble<T, P>>();

    public void Raise(T parameter, P parameter2)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(parameter, parameter2);
    }

    public void RegisterListener(GEListenerGenericoDoble<T, P> listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GEListenerGenericoDoble<T, P> listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
